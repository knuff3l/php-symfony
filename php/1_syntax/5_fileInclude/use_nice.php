<?php
/*
 * FileInclude - Beispiel
 *
 * PHP-Skript, das eine Funktion aus einer anderen Datei nutzt
 */

/* Verschiedene Varianten, um Dateien einzubinden*/
	//require 'pfad/datei.php';      // wenn die Datei nicht eingebunden werden kann, folgt ein Fehler
	//include 'pfad/datei.php';      // wenn die Datei nicht eingebunden werden kann, folgt eine Warnung
	//require_once 'pfad/datei.php'; // bindet die Datei nur ein, wenn sie zuvor noch nicht eingebunden wurde
	//include_once 'pfad/datei.php'; // bindet die Datei nur ein, wenn sie zuvor noch nicht eingebunden wurde

require_once 'nice.php';

$klasse = "CS16-1";

echo $klasse.", ".stayNice();
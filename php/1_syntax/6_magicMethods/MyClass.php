<?php
/*
 * MagicMethods - Beispiel
 *
 * Ermöglichen es auf bestimmte Ereignisse zu reagieren, wenn diese bei einem Objekt eintreten
 *
 * weitere magicMethods: __destruct(), __call(), __callStatic(), __get(), __set(), __isset(), __unset(), __sleep(), __wakeup(), __toString(), __invoke(), __set_state(), __clone() und __debugInfo()
 */

class MyClass {

	private $text;

	public function __construct($text) {
		$this->text=$text;
	}

	public function getText() {
		return $this->text;
	}
}

$myObject = new MyClass("Beispiel der magic method __construct()");
echo $myObject->getText();
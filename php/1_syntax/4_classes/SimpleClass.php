<?php
/*
 * Klassen - Beispiel
 */

class SimpleClass {

	public const KLASSEN_KONSTANTE = 420; // Klassenkonstante

	public $greet = 'Hallo';  // Klassenvariable mit Standardwert
	private $var;   // Sichtbarkeit private

	public function greetPeople(){
		echo $this->greet.' Leute'.PHP_EOL;
	}
}

$mySimple = new SimpleClass();

echo $mySimple->greet.PHP_EOL;

$mySimple->greetPeople();

echo $mySimple::KLASSEN_KONSTANTE;
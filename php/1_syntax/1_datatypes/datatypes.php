<?php
/*
 * Datentypen - Beispiel
 *
 * PHP nutzt dynamische Typisierung
 *
 * Typen für Methodenargumente und Rückgabewerte möglich (seit 7.0 auch primitive Typen)
 */

$wahrheitswert = true;         // boolean
$ganzzahl = 5;                 // integer
$kommazahl = 4.71;             // float (oder double)
$zeichenkette = 'foo';         // string

// Zusammengesetzte Datentypen
$feld = array('foo', 'bar');   // array
$objekt = new stdClass();      // object
$funktion = function () {      // callable
	echo 'hello world!';
};

// Spezielle Datentypen
$resource = fopen("foo", "w"); // resource
$variable = NULL;              // NULL
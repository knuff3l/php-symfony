# PHP

- `1_syntax`
  - Codebeispiele zur PHP-Syntax
- `2_features`
  - Codebeispiele zu PHP-Features
- `3_good_practices`
  - Codebeispiele zu PHP-Good Practices
<?php
/*
 * DateTime - Beispiel
 */

// aktuelle/s Datum/Uhrzeit
$datetime = new DateTime();
echo $datetime->format('d.m.Y H:i:s').PHP_EOL;

// selbst übergeben, in konformen Format
$datetime = new DateTime('05.04.2019 08:00:00');
echo $datetime->format('d.m.Y H:i:s').PHP_EOL;

// selbst übergeben, eigenes Format
$datetime = DateTime::createFromFormat('M j, Y H:i:s', 'Jan 2, 2014 23:04:12');
echo $datetime->format('d.m.Y H:i:s').PHP_EOL;
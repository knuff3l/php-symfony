<?php
/*
 * DateTimeZone - Beispiel
 */

/*
 * Standard-Zeitzone setzen
 */
// entweder in der php.ini mit
	// date.timezone = 'America/New_York';
// oder im Skript mit
	// date_default_timezone_set('Europe/Berlin');

// Zeitzonen-Objekt erzeugen
$timezone = new DateTimeZone('America/New_York');

// Zeitzone nutzen, um neues DateTime-Objekt zu erzeugen
$datetime = new DateTime('2014-08-20', $timezone);
var_dump($datetime);

// Zeitzone später anpassen
$datetime->setTimezone(new DateTimeZone('Asia/Hong_Kong'));

echo PHP_EOL;
echo PHP_EOL;
var_dump($datetime);


<?php
/*
 * DatePeriod - Beispiel
 */

$start = new DateTime();
$interval = new DateInterval('P2W');

// DatePeriod mit optionalem vierten Parameter
// Als dritter Parameter kann auch ein Enddatum übergeben werden
$period = new DatePeriod($start, $interval, 3, DatePeriod::EXCLUDE_START_DATE);

foreach ($period as $nextDateTime) {
	echo $nextDateTime->format('Y-m-d H:i:s'), PHP_EOL;
}
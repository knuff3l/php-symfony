<?php
/*
 * DateInterval - Beispiel
 */

// DateTime Instanz erzeugen
$datetime = new DateTime();

// Zwei-Wochen-Intervall erzeugen
$interval = new DateInterval('P2W');

// DateTime Instanz anpassen
$datetime->add($interval);
echo $datetime->format('d.m.Y H:i:s').PHP_EOL;

// Intervall aus String erzeugen
$dateInterval = DateInterval::createFromDateString('-1 day');

$gestern = new DateTime();
$gestern->add($dateInterval);
echo $gestern->format('d.m.Y H:i:s');
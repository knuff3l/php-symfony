<?php
/*
 * StreamFilter - Beispiel
 */
$handle = fopen('data.txt', 'rb');
stream_filter_append($handle, 'string.toupper');
while(feof($handle) !== true) {
	echo fgets($handle); // <-- Gibt Text in Großbuchstaben aus
} fclose($handle);

echo PHP_EOL;

/*
 * Alternativ
 *
 * Hilfreich bei Funktionen, denen man kein Filter anhängen kann (z.B. file() oder fpassthru())
 */
$handle = fopen('php://filter/read=string.toupper/resource=data.txt', 'rb');
while(feof($handle) !== true) {
	echo fgets($handle); // <-- Gibt Text in Großbuchstaben aus
} fclose($handle);

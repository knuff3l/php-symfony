<?php
/*
 * Trait - Beispiel
 *
 * Beispiel-Klasse, die einen Trait nutzt
 */
include "MyTrait.php";

class ExampleClass {
	use MyTrait;
}
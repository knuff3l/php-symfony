<?php
/*
 * Generator - Beispiel
 *
 * Ein einfacher Generator
 */
function myGenerator(){
	yield 'wert1';
	yield 'wert2';
	yield 'wert3';
}

foreach (myGenerator() as $value){
	echo $value.PHP_EOL;
}

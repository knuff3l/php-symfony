<?php
/*
 * Interface - Beispiel
 *
 * Klasse, die ein Interface implementiert
 */
require_once 'MyInterface.php';

class TestClass implements MyInterface {

	public function sayHi() {
		echo 'Hallo';
	}
}

$obj = new TestClass();

$obj->sayHi();

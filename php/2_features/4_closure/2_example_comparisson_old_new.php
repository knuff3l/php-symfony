<?php
/*
 * Closure - Beispiel
 *
 * Vergleich zum Aufruf bevor es Closures gab
 */

/** bevor es Closures gab ***/
function incrementNumber($number) {
	return $number + 1;
}

$numbersPlusOne = array_map('incrementNumber', [1, 2, 3]);

print_r($numbersPlusOne);

//--------------------------------------------------------//

/** mit Closure ***/
$numbersPlusOne = array_map(function ($number) {
	return $number + 1;
}, [1, 2, 3]);

print_r($numbersPlusOne);


<?php
/*
 * Closure - Beispiel
 *
 * Hier handelt es sich um eine anonyme Funktion (wird in PHP synonym zu Closure genutzt)
 */
$closure = function ($name){
	return sprintf('Hallo %s',$name);
};

echo $closure('Maria');


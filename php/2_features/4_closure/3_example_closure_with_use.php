<?php
/*
 * Closure - Beispiel
 *
 * Kapselung von Zuständen mit use()
 *
 * Ist auch mit bindTo() möglich
 */

function enclosePerson($name) {
	return function ($doCommand) use ($name) {
		return sprintf('Maria sagt: %s, %s'.PHP_EOL, $name, $doCommand);
	};
}
// String "Tobias" in Closure kapseln
$tobias = enclosePerson('Tobias');

// Closure mit Befehl aufrufen
echo $tobias('hol mir Kaffee!');
echo $tobias('mach den Code!');


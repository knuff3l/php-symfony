# Vortrag zu PHP und Symfony
Dieses Repository dient für unseren Vortrag und gibt einen Einblick in PHP und Symfony.
## Verzeichnisstruktur
- `php`
  - Beispiele zu *Syntax, Features* und *Good Practices*
- `symfony`
   - Einführungsbeispiel zum Erstellen eines Symfony-Projektes

Beide Unterverzeichnisse enthalten weitere README-Dateien zur Erläuterung der jeweiligen Struktur.
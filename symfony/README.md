# Symfony
Dies ist eine kleine Einführung zum Erstellen eines Symfony-Projektes.

Wir richten uns dabei an die offzielle Dokumentation, die unter nachfolgendem Link zu finden ist:

Link zu [Symfony 4.2 - Getting Started](https://symfony.com/doc/current/index.html#gsc.tab=0)

## Voraussetzungen

Folgende Technologien müssen auf dem Rechner installiert und eingerichtet sein:
- PHP
- Composer

## Schritt-für-Schritt

### Projekt erstellen
1. Neues Projekt erstellen (in der Kommandozeile)
```bash
composer create-project symfony/website-skeleton my-project
```
2. Lokalen Webserver starten & Ergebnis betrachten (in der Kommandozeile)
```bash
cd my-project

php bin/console server:run
```
Dann `http://localhost:8000/` im Browser aufrufen.

### Ordner-Struktur

- `config/`
  - Enthält die Konfiguration
  - Beispielsweise für: routes, services und packages
- `src/`
  - Hier lebt der eigene Anwendungs-PHP-Code
- `templates/`
  - Hier befinden sich die Twig Templates
  - Templates für HTML-Seiten mit der Twig-Engine
- `bin/`
  - Hier befinden sich bin/console und weitere andere Dateien
- `var/`
  - Hier werden automatisch erstellte Dateien gespeichert, wie Cache-Dateien (var/cache/) und Logs (var/log/)
- `vendor/`
  - Hier leben Bibliotheken von Drittanbietern 
  - Diese werden über den Composer-Paketmanager heruntergeladen
- `public/`
  - Dies ist das Wurzelverzeichnis des Projektes
  - Hier legt man alle öffentlich zugänglichen Dateien ab

Wenn man neue Pakete installiert, werden bei Bedarf automatisch neue Verzeichnisse erstellt.
### Erste Seite erstellen

#### Vorbetrachtung
Der Vorgang zum Erstellen einer neuen Seite (ob HTML oder JSON-Endpoint) ist immer gleich:

1. __Route erstellen:__ Eine Route ist die URL (z.B. /about) zu der Seite und zeigt auf einen Controller.
2. __Controller erstellen:__ Ein Controller ist die PHP-Funktion, die die Seite aufbaut. Sie nimmt die eingehenden Request-Informationen entgegen und erstellt daraus ein Symfony-*Response*-Objekt, welches HTML-Inhalte, einen JSON-String oder sogar eine Binärdatei, wie ein Bild oder ein PDF enthalten kann.

#### Beispiel
Wir erstellen eine Seite - `/lucky/number`, die eine (zufällige) Glückszahl anzeigt.

1. Controller-Klasse und Controller-Methode erstellen
```php
<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class LuckyController
{
    public function number()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}
```
2.  Route unter config/routes.yaml erstellen
```yaml
# config/routes.yaml

# the "app_lucky_number" route name is not important yet
app_lucky_number:
    path: /lucky/number
    controller: App\Controller\LuckyController::number
```
3. Server starten und Seite aufrufen
    - `php bin/console server:run`
    - Dann `http://localhost:8000/lucky/number` aufrufen
4. Routing mit Annotations
    - Annotation Package laden: `composer require annotations`
    - Controller anpassen
    ```php
        // src/Controller/LuckyController.php

    // ...
    + use Symfony\Component\Routing\Annotation\Route;
    
    class LuckyController
    {
    +     /**
    +      * @Route("/lucky/number")
    +      */
        public function number()
        {
            // this looks exactly the same
        }
    }
    ```
5. Wie 3., der Server muss aber nicht neu gestartet werden, wenn er schon läuft
6. Template nutzen
    - Controller muss von `AbstractController` erben
    ```
        // src/Controller/LuckyController.php
    
    // ...
    + use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    
    - class LuckyController
    + class LuckyController extends AbstractController
    {
        // ...
    }
    ```
    - `render()` nutzen
    ```
        // src/Controller/LuckyController.php
    
    // ...
    class LuckyController extends AbstractController
    {
        /**
         * @Route("/lucky/number")
         */
        public function number()
        {
            $number = random_int(0, 100);
    
            return $this->render('lucky/number.html.twig', [
                'number' => $number,
            ]);
        }
    }
    ```
    - Neues Template unter `templates/lucky/` anlegen
    ```twig
        {# templates/lucky/number.html.twig #}
    
    <h1>Your lucky number is {{ number }}</h1>
    ```
    